#include<stdio.h>

int main(){
	
	int a = 20;
	int *p = &a;
	
	printf("value of a=%d\n",a);
	printf("value of *p=%d\n",*p);
	printf("address of &a=%u\n",&a);
	printf("value of p=%d\n",p);
	printf("address of &p=%u\n",&p);
	(*p)++;
	
	printf("value of a=%d\n",a);
	printf("address of &a=%u\n",&a);
	
}
