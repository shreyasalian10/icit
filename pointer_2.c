#include<stdio.h>

int main(){
	
	int a = 10;
	int *p;
	int **q;
	int ***r;
	p = &a;
	q = &p;
	r = &q;
	printf("value of a=%d\n",a);
	printf("value of &a=%d\n",&a);
	printf("value of *p=%d\n",*p);
	printf("value of &p=%d\n",&p);
	printf("value of **q=%d\n",**q);
	printf("value of &q=%d\n",&q);
	printf("value of ***r=%d\n",***r);
	printf("value of &r=%d\n",&r);
	
	(*p)++;
		printf("value of a=%d\n",a);
}


